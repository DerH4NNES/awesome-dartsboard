'use client';
import '@/scss/dartboard.scss'
import '@/scss/main.scss'
import {Navigation} from "@/components/UI/Navigation";

export default function Home() {

    return (
        <div className={'container-fluid p-0'}>
            <Navigation />
        </div>
    )
}
