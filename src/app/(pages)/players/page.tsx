'use client';
import {useEffect, useState} from 'react';
import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {LocalStorageUtils} from "@/components/Utils/DatabaseUtils";
import {FaTimes} from "react-icons/fa";
import {PageHeading} from "@/components/UI/PageHeading";
import {Navigation} from "@/components/UI/Navigation";

export default function PlayerForm() {
    const [players, setPlayers] = useState<GenericPlayer[]>([]);
    const [name, setName] = useState('');

    const handleNameChange = (e: any) => {
        setName(e.target.value);
    };

    useEffect(() => {
        if (!players.length) {
            const loadedPlayers = LocalStorageUtils.loadPlayers();
            setPlayers(loadedPlayers);
        }
    }, []);

    const handleAddPlayer = () => {
        if (name.trim() !== '') {
            const newPlayers = [...players, new GenericPlayer(name)];
            setName('');
            setPlayers(newPlayers);
            localStorage.setItem('players', JSON.stringify(newPlayers));
        }
    };

    const removePlayer = (playerId: string) => {
        const updatedPlayers = players.filter(player => player.uuid !== playerId);
        setPlayers(updatedPlayers);
        localStorage.setItem('players', JSON.stringify(updatedPlayers));
    };

    return (
        <div>
            <PageHeading text="Spielerliste"/>
            <Navigation/>
            <div className="container mt-4">
                <div className="input-group">
                    <input onChange={handleNameChange} value={name} type="text" className="form-control"
                           placeholder="Spielername"/>
                    <button onClick={handleAddPlayer} className="btn btn-outline-secondary" type="button">Spieler
                        hinzufügen
                    </button>
                </div>


                <table className="table table-striped mt-5 text-start">
                    <thead>
                    <tr>
                        <th>Spielername</th>
                        <th>UUID</th>
                        <th>Aktion</th>
                    </tr>
                    </thead>
                    <tbody>
                    {players.map((p, index) => {
                        return <tr key={index}>
                            <td>{p.name}</td>
                            <td>{p.uuid}</td>
                            <td>
                                <button
                                    className="btn btn-outline-danger d-flex justify-content-center align-items-center"
                                    onClick={() => removePlayer(p.uuid)}><FaTimes/></button>
                            </td>
                        </tr>
                    })}
                    </tbody>
                </table>
            </div>
        </div>
    )
        ;
}