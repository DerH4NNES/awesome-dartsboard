'use client';
import {GAMES, GameUtils} from "@/components/Utils/GameUtils";
import Link from "next/link";
import {PageHeading} from "@/components/UI/PageHeading";
import {Navigation} from "@/components/UI/Navigation";

export default function Games() {

    return (
        <div>
            <PageHeading text="Spiele"/>
            <Navigation/>
            <div className="container mt-4">
                <div className="row">
                    {Object.keys(GAMES).map((gameName, index) => {
                        return (
                            <div key={index} className="col-md-2 col-12">
                                <div className="card text-center m-2">
                                    <div className="card-body">
                                        <h5 className="card-title">{GameUtils.getGameByName(gameName)?.displayName}</h5>
                                        {/*<p className="card-text">Game Text</p>*/}
                                        <Link href={`/game/${gameName}/create`}>
                                            <span className="btn btn-primary">Los geht's!</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    );
}