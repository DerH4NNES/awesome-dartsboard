'use client';

import {GameUtils} from "@/components/Utils/GameUtils";
import {PageHeading} from "@/components/UI/PageHeading";
import {GenericPlayerSelection} from "@/components/Games/Generic/GenericPlayerSelection";
import {useEffect, useState} from "react";
import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {BoardWrapper} from "@/components/Boards/BoardWrapper";
import {LocalStorageDatabase} from "@/components/Database/LocalStorageDatabase";

export default function GamePage({params}: { params: { gameName: string, roundId: string } }) {
    const [selectedPlayers, setSelectedPlayers] = useState<GenericPlayer[]>([]);
    const [gameSettings, setGameSettings] = useState<any>(null);

    useEffect(() => {
        const db = new LocalStorageDatabase();
        db.readArray('rounds').then((e) => {
            const element = e.find((round: any) => {
                return round.id === params.roundId;
            })

            if (element) {
                setGameSettings(element);
            }
        })
    }, []);

    if (!gameSettings) {
        return <div className="loader" />
    }

    const onSelectCallback = (players: GenericPlayer[]) => {
        setSelectedPlayers(players);
    }

    const game = GameUtils.getGameByName(params.gameName)
    return (
        <>

            {selectedPlayers.length === 0 &&
                <div className="display-4">
                    <PageHeading text="Spielerauswahl"/>
                    <GenericPlayerSelection onSelectCallback={onSelectCallback}/>
                </div>
            }
            {
                selectedPlayers.length !== 0 &&
                <BoardWrapper gameSettings={gameSettings} game={game} players={selectedPlayers}/>
            }
        </>)
}