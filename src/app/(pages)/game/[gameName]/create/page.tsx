'use client';

import {GameUtils} from "@/components/Utils/GameUtils";
import {PageHeading} from "@/components/UI/PageHeading";
import {Navigation} from "@/components/UI/Navigation";
import {GameSettings} from "@/components/UI/GameSettings/GameSettings";


export default function GamePage({params}: { params: { gameName: string } }) {
    const gName = GameUtils.getGameByName(params.gameName)
    return (
        <div>
            <PageHeading text={gName.displayName}/>
            <Navigation />
            <GameSettings gameName={params.gameName}/>
        </div>)
}