'use client';
import '@/scss/main.scss'
import {useSettings} from "@/components/Boards/useSettings";
import {DartsBoardDrivers} from "@/components/Boards/DartsBoardDrivers";
import {Navigation} from "@/components/UI/Navigation";
import {LocalStorageDatabase} from "@/components/Database/LocalStorageDatabase";
import {useState} from "react";
import toast from "react-hot-toast";
import {PageHeading} from "@/components/UI/PageHeading";

export default function SettingsPage() {
    const settings = useSettings();

    const [driver, setDriver] = useState<null | string>(null)

    const save = () => {
        new LocalStorageDatabase().writeObject('settings', {
            driver: driver
        }).then(() => {
            toast.success('Treiber gespeichert');
        })
    }

    if (!settings) {
        return <div className="loader" />;
    }

    return (
        <div>
            <PageHeading text="Einstellungen"/>
            <Navigation/>
            <div className="container text-center mt-4">
                <label>
                    <span>Treiber</span>
                    <select defaultValue={settings.driver} onChange={(e) => setDriver(e.target.value)}
                            className="form-select"
                            aria-label="Default select example">
                        {Object.keys(DartsBoardDrivers).map((value, index) => {
                            return (
                                <option key={index} value={value}>{value}</option>
                            )
                        })}
                    </select>
                </label>
                <br/>
                <button className="btn btn-primary btn-lg" onClick={() => save()}>Speichern</button>
            </div>
        </div>
    )
}