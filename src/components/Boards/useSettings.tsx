import {useEffect, useState} from "react";
import {LocalStorageDatabase} from "@/components/Database/LocalStorageDatabase";

export function useSettings() {
    const [settings, setSettings] = useState<any>(null);

    useEffect(() => {
        const db = new LocalStorageDatabase();

        db.readObject('settings', {
            driver: 'DEBUG'
        })
            .then((dbSettings) => {
                setSettings(dbSettings)
            });
    }, []);

    return settings;
}