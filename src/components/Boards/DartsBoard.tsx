'use client';
import {BoardAbstract} from "@/components/Boards/BoardAbstract";
import {createElement} from "react";
import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {InitButton} from "@/components/UI/InitButton";
import {Game01} from "@/components/Games/01/Game01";
import Link from "next/link";
import {GameFunctionType} from "@/components/Games/GameFunctionType";


export default function DartsBoard({board, players, game, gameSettings}: {
    board: BoardAbstract,
    players: GenericPlayer[],
    game: GameFunctionType,
    gameSettings: any
}) {

    const Games = {
        '01': Game01,
    };

    if (!players || !players.length)
        return <div className="container-fluid">
            <h1>Keine Spieler</h1>
            <Link href="/players">Spieler hinzufügen</Link>
        </div>;

    return createElement(game, {
        players,
        settings: gameSettings,
        initButton: <InitButton initFunction={board.init.bind(board)}></InitButton>
    })
}