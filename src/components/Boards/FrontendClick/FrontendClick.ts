import {BoardAbstract} from "../BoardAbstract";

export class FrontendClick extends BoardAbstract {

    async init() {
        const paths = document.getElementsByClassName('scoreable');
        for (let i = 0; i < paths.length; i++) {
            // @ts-ignore - Passing onHit to event listener
            paths[i]['onHit'] = this.onHit.bind(this);
            paths[i].addEventListener('click', function () {
                // @ts-ignore
                const elem = document.getElementById(this.id);
                // @ts-ignore
                this.onHit(this.id);
                if (!elem) return;
                elem.classList.add('fadeTransition');

                if (!elem) return;
                setTimeout(function () {
                    elem.classList.remove('fadeTransition');
                }, 1000);
            });
        }

        return true;
    }
}

