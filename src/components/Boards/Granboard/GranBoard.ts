import {BoardAbstract} from "../BoardAbstract";
import toast from "react-hot-toast";
import {BTSignalMapping} from "@/components/Boards/Granboard/BTSignalMapping";
import {GranBoard132} from "@/components/Boards/Granboard/Bluetooth/GranBoard132";

export class GranBoard extends BoardAbstract {
    granboard132: GranBoard132 = new GranBoard132()

    dartsMapping(value: string) {
        // @ts-ignore
        return BTSignalMapping[value.replace('@', '')];
    }

    handleNotifications(event: any) {
        let value = event.target.value;
        const decoded = new TextDecoder()
            .decode(value)
            .replace('@', '');

        const mappedValue = this.dartsMapping(
            decoded
        );

        if (decoded === 'BTN') {
            this.onPressContinue();
            return;
        }
        if (!mappedValue) {
            console.log("NOT MAPPED VALUE", value, mappedValue);
        }

        if (mappedValue === 'OUT') {
            this.onHit('OUT');
            return;
        }

        if (mappedValue) {
            const hitValue = mappedValue.replace('_', '').toLowerCase();

            this.onHit(hitValue);
        }
    }

    async init() {
        try {
            return await this.granboard132.init(this.handleNotifications.bind(this));
        } catch (error: any) {
            toast.error('Fehler beim Verbinden mit dem Gerät');
            console.log(error);
            return false;
        }
    }
}