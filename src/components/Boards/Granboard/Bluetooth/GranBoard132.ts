import toast from "react-hot-toast";

export class GranBoard132 {
    serviceUUID = '442F1570-8A00-9A28-CBE1-E1D4212D53EB'.toLowerCase();
    characteristicUUID = '442F1571-8A00-9A28-CBE1-E1D4212D53EB'.toLowerCase();
    writeable = '442F1572-8A00-9A28-CBE1-E1D4212D53EB'.toLowerCase();

    device?: BluetoothDevice;
    characteristic?: BluetoothRemoteGATTCharacteristic;

    async watchConnection() {
        if (!this.device) return;

        this.device.addEventListener('gattserverdisconnected', async () => {
            toast.error('Bluetooth-Verbindung getrennt.');
        });
    }

    async connectToDevice() {
        console.log("Try to connect to device");
        try {
            const options = {
                filters: [{services: [this.serviceUUID]}],
            };

            this.device = await navigator.bluetooth.requestDevice(options) as BluetoothDevice;
            // @ts-ignore
            const server = await this.device.gatt.connect();
            const service = await server.getPrimaryService(this.serviceUUID);
            this.characteristic = await service.getCharacteristic(this.characteristicUUID);
            // Lichter aus
            // await (await service.getCharacteristic(this.writeable)).writeValue(new Buffer(hex))

            // await (await service.getCharacteristic(this.writeable)).writeValue(new Buffer([0x0000, 0x01, 0x59, 0xfd, 0x00]))
            // const hex = parseInt('0x15E', 16)

            /*
            Start
            Regenbogen
            Langsam pulse
            Hellblau
            Rosa
            
             */

            const Sleep = (milliseconds: number) => {
                return new Promise(resolve => setTimeout(resolve, milliseconds));
            }

            const pulseBunt = async () => {
                await writeable.writeValueWithResponse(new Buffer([0x16, 0x03, 0x02, 0x01, 0x00, 0x04, 0x02, 0x01, 0x00])).then((d) => console.log(d))
            }

            const pausePulse = async () => {
                await writeable.writeValueWithResponse(new Buffer([])).then((d) => console.log(d))
            }

            const unknownPulseFF = async () => {
                await writeable.writeValueWithResponse(new Buffer([0xFF])).then((d) => console.log(d))
            }

            const unknownPulse010100 = async () => {
                await writeable.writeValueWithResponse(new Buffer([0x01, 0x01, 0x00])).then((d) => console.log(d))
            }

            const unknownPulseLong = async () => {
                await writeable.writeValueWithResponse(new Buffer([0x01, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                ])).then((d) => console.log(d))
            }

            const unknownPulse0001 = async () => {
                await writeable.writeValueWithResponse(new Buffer([0x00, 0x01])).then((d) => console.log(d))
            }

            const unknownPulseLong2 = async () => {
                await writeable.writeValueWithResponse(new Buffer([0x01, 0x00, 0x01, 0x00, 0x00, 0x11, 0x11, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])).then((d) => console.log(d))
            }

            const unknownPulse010101 = async () => {
                await writeable.writeValueWithResponse(new Buffer([0x01, 0x01, 0x01])).then((d) => console.log(d))
            }

            const unknownPulse010103 = async () => {
                await writeable.writeValueWithResponse(new Buffer([0x01, 0x01, 0x03])).then((d) => console.log(d))
            }

            const unknownPulseLong3 = async () => {
                await writeable.writeValueWithResponse(new Buffer([0x00, 0x0e, 0x00, 0x00, 0x00, 0xf4, 0x01]))
            }

            const unknownPulseLong4 = async () => {
                await writeable.writeValueWithResponse(new Buffer([0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                ])).then((d) => console.log(d))
            }

            const writeable = await service.getCharacteristic(this.writeable);
            const fn = async () => {
                // Hallo 1. Befehl
                await pulseBunt();
                await Sleep(2000);
                await pausePulse();
                // await Sleep(2000);
                // await unknownPulseFF();
                // await Sleep(2000);
                // await unknownPulse010100();
                // await Sleep(2000);
                // await unknownPulse010100();
                // await Sleep(2000);
                // await unknownPulse0001();
                // await Sleep(2000);
                // await unknownPulseLong2();
                // await Sleep(2000);
                // await unknownPulse010101();
                // await Sleep(2000);
                // await unknownPulse010103();
                // await Sleep(2000);
                // await unknownPulseLong3();
                // await Sleep(2000);
                // await unknownPulseLong4();


                // 2. Befehl Orange
                // await Sleep(5000);
                // new MediaPlayer().playAudio();
                // await writeable.writeValueWithResponse(new Buffer([])).then((d) => console.log(d))
                //
                // // 2. Befehl Orange
                // await Sleep(5000);
                // new MediaPlayer().playAudio();
                // await writeable.writeValueWithResponse(new Buffer([])).then((d) => console.log(d))
                //
                // await Sleep(5000);
                // new MediaPlayer().playAudio();
                // await writeable.writeValueWithResponse(new Buffer([0x01, 0x01, 0x00])).then((d) => console.log(d))
                //
                // await Sleep(5000);
                // new MediaPlayer().playAudio();
                // await writeable.writeValueWithResponse(new Buffer(hex)).then((d) => console.log(d))
                //
                //
                // await Sleep(5000);
                // new MediaPlayer().playAudio();
                // await writeable.writeValueWithResponse(new Buffer([])).then((d) => console.log(d))
                // await writeable.writeValueWithResponse(new Buffer([0x01, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                // ])).then((d) => console.log(d))
                // pulse
                // await writeable.writeValueWithResponse(new Buffer([ 0xff])).then((d) => console.log(d))
                // await writeable.writeValueWithResponse(new Buffer([0x01, 0x01, 0x01])).then((d) => console.log(d))

                // Orange blink -> Aus
                // await writeable.writeValueWithResponse(new Buffer([0x01, 0x01, 0x00])).then((d) => console.log(d))

                // await writeable.writeValueWithResponse(new Buffer([0x01, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                // ])).then((d) => console.log(d))


            }

            setTimeout(() => {
                console.log("sent");
                fn()
            }, 4000);


            toast.success('Verbunden mit Gerät: ' + this.device.name);
        } catch (error) {
            console.error('Fehler beim Verbinden mit dem Gerät:', error);
        }
    }

    async init(notificationCallback: (event: any) => void) {
        try {
            await this.connectToDevice();

            if (this.characteristic === undefined) {
                toast.error('Internal bt error');
                return false;
            }

            this.characteristic.startNotifications().then((_: BluetoothRemoteGATTCharacteristic) => {
                // @ts-ignore
                this.characteristic.addEventListener('characteristicvaluechanged', notificationCallback);
            });
            return true;
        } catch (error: any) {
            toast.error('Fehler beim Verbinden mit dem Gerät');
            console.log(error);
            return false;
        }
    }

    async disconnectFromDevice() {
        if (this.device?.gatt?.connected) {
            this.device.gatt.disconnect();
            toast.success('Verbindung zum Gerät getrennt');
        }
    }
}