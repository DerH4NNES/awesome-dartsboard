export const BTSignalMapping = {
    '2.6': 'D1',
    '2.5': 'S1',
    '2.4': 'T3',
    '2.3': '_S1',
    '9.2': 'S2',
    '8.2': 'D2',
    '9.0': 'T2',
    '9.1': '_S2',
    '7.2': 'S3',
    '8.4': 'D3',
    '7.0': 'T3',
    '7.1': '_S3',
    '0.5': 'S4',
    '0.6': 'D4',
    '0.3': 'T4',
    '0.1': '_S4',
    '6.3': 'D19',
    '8.5': 'S19',
    '6.0': 'T19',
    '6.1': '_S19',
    '1.6': 'D18',
    '1.5': 'S18',
    '1.4': 'T18',
    '1.2': '_S18',
    '4.5': 'D13',
    '0.4': 'S13',
    '0.2': 'T13',
    '0.0': '_S13',
    '4.4': 'D6',
    '1.3': 'S6',
    '1.1': 'T6',
    '1.0': '_S6',
    '4.3': 'D10',
    '2.2': 'S10',
    '2.1': 'T10',
    '2.0': '_S10',
    '4.2': 'D15',
    '3.2': 'S15',
    '3.1': 'T15',
    '3.0': '_S15',
    '8.3': 'D17',
    '10.2': 'S17',
    '10.0': 'T17',
    '10.1': '_S17',
    '8.6': 'D7',
    '11.4': 'S7',
    '11.2': 'T7',
    '11.1': '_S7',
    '11.6': 'D16',
    '11.5': 'S16',
    '11.3': 'T16',
    '11.0': '_S16',
    '6.6': 'D8',
    '6.5': 'S8',
    '6.4': 'T8',
    '6.2': '_S8',
    '7.6': 'D11',
    '7.5': 'S11',
    '7.4': 'T11',
    '7.3': '_S11',

    '10.6': 'D14',
    '10.5': 'S14',
    '10.4': 'T14',
    '10.3': '_S14',

    '9.6': 'D9',
    '9.5': 'S9',
    '9.4': 'T9',
    '9.3': '_S9',
    '5.6': 'D12',
    '5.5': 'S12',
    '5.3': 'T12',
    '5.0': '_S12',
    '4.6': 'D5',
    '5.4': 'S5',
    '5.2': 'T5',
    '5.1': '_S5',
    '3.3': '_S20',
    '3.4': 'T20',
    '3.5': 'S20',
    '3.6': 'D20',
    '4.0': 'DB',
    '8.0': 'B',
    'OUT': 'OUT',
    'GB8;102': 'Verbunden mit GB8;102'
};