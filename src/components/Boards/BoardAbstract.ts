import {DartsBoardEvents} from "@/components/Boards/DartsBoardEvents";

export abstract class BoardAbstract {

    parseFactor(prefix: string) {
        switch (prefix) {
            case 's':
                return 1;
            case 'd':
                return 2;
            case 't':
                return 3;
            default:
                return NaN;
        }
    }

    onHit(value: string) {
        const elem = document.getElementById(value.toLowerCase().replace('_', ''));

        if (!elem && value !== 'OUT') {
            console.warn('Mapping missing for field', value.toLowerCase())
            return;
        }

        const prefix = value.slice(0, 1);

        if (value === 'OUT') {
            DartsBoardEvents.emit('ON_HIT', {
                prefix: 'S',
                value: 0,
                raw: 'OUT',
                factor: 1
            })
        } else if (value === 'BTN') {
            DartsBoardEvents.emit('ON_PRESS_NEXT_PLAYER')
        } else if (value.toUpperCase() === 'DB') {
            DartsBoardEvents.emit('ON_HIT', {
                prefix: value.slice(0, 1),
                value: 25,
                raw: value,
                factor: this.parseFactor(prefix)
            })
        } else if (value.toUpperCase() === 'B') {
            DartsBoardEvents.emit('ON_HIT', {
                prefix: 's',
                value: 25,
                raw: value,
                factor: this.parseFactor('s')
            })
        } else {
            DartsBoardEvents.emit('ON_HIT', {
                prefix: value.slice(0, 1),
                value: parseInt(value.slice(1, value.length)),
                raw: value,
                factor: this.parseFactor(prefix)
            })
        }

        if (elem) {
            elem.classList.add('fadeTransition');
            setTimeout(function () {
                elem.classList.remove('fadeTransition');
                elem.classList.add('fadeTransitionReverse');
            }, 1000);
        }
    }

    onPressContinue() {
        DartsBoardEvents.emit('PRESS_CONTINUE');
    }

    async init() {
        return true;
    }
}