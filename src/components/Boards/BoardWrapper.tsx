'use client';
import DartsBoard from "@/components/Boards/DartsBoard";
import {Toaster} from "react-hot-toast";
import {useEffect, useState} from "react";
import {BoardAbstract} from "@/components/Boards/BoardAbstract";
import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {useSettings} from "@/components/Boards/useSettings";
import {DartsBoardDrivers} from "@/components/Boards/DartsBoardDrivers";
import {GameFunctionType} from "@/components/Games/GameFunctionType";
import {DartsBoardEvents} from "@/components/Boards/DartsBoardEvents";


export function BoardWrapper({players, game, gameSettings}: {
    players: GenericPlayer[],
    game: GameFunctionType,
    gameSettings: any
}) {
    const [board, setBoard] = useState<null | BoardAbstract>(null);
    const settings = useSettings();

    useEffect(() => {
        if (!board || !settings) {
            return;
        }
        const video: HTMLElement | null = document.getElementById('videoPlayer');
        const videoSource: HTMLElement | null = document.getElementById('videoSource');
        const videoWrapper: HTMLElement | null = document.getElementById('videoWrapper');
        DartsBoardEvents.on('MEDIA_PLAY_CLIP', handleClip)

        function handleClip(videoPath: string) {
            if (videoSource && video && videoPath && videoWrapper) {
                videoSource.setAttribute('src', videoPath);
                videoWrapper.style.display = 'block';
                (video as HTMLVideoElement).load();
                (video as HTMLVideoElement).play();
                video.addEventListener('ended', () => videoWrapper.style.display = 'none', false);
            }
        }

        return () => {
            DartsBoardEvents.off('MEDIA_PLAY_CLIP', handleClip)
        }
    });


    useEffect(() => {
        if (settings) {
            // @ts-ignore
            setBoard(new DartsBoardDrivers[settings.driver]());
        }
    }, [settings]);

    if (!settings) return <div className="loader" />;

    if (board === null) {
        return <div className="loader" />;
    }

    return (
        <>
            <div
                id="videoWrapper"
                style={{
                    zIndex: 1056,
                    position: 'absolute',
                    top: '0',
                    opacity: 0.95,
                    left: '0',
                    width: '100vw',
                    height: '100vh',
                    background: "black",
                    display: 'none'
                }}>
                <video
                    style={{
                        width: "100%",
                        height: "100%",
                        zIndex: 1600
                    }}
                    id="videoPlayer"
                >
                    <source id="videoSource" type="video/mp4"/>
                    Your browser does not support the video tag.
                </video>
            </div>
            <Toaster/>
            <DartsBoard board={board} game={game} gameSettings={gameSettings} players={players}/>
        </>
    )
}