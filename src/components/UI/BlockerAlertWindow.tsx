'use client';
import {useEffect, useRef} from "react";

export interface BlockerAlertWindowProps {
    title: string,
    actionText: string,
    emitter: () => void,
    content?: any
}

export function BlockerAlertWindow({title, actionText, emitter, content}: BlockerAlertWindowProps) {

    const ref = useRef<any | null>(null);

    useEffect(() => {
        const Modal = require('bootstrap/js/src/modal');

        if (ref?.current) {
            const modal = new Modal.default(ref.current)
            modal.show();

            return () => {
               modal.hide();
            }
        }
    }, [ref]);

    return (
        <div ref={ref} className="modal modal-lg fade show">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content h-100">
                    <div className="modal-header">
                        <h5 className="modal-title">{title}</h5>
                    </div>
                    {!!content && <div className="modal-body">
                        {content}
                    </div>
                    }
                    <div className="modal-footer">
                        <button onClick={() => emitter()} type="button"
                                className="btn btn-outline-primary btn-lg">{actionText}</button>
                    </div>
                </div>
            </div>
        </div>);
}