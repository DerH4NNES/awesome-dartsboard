import {BlockerAlertWindow} from "@/components/UI/BlockerAlertWindow";
import {useState} from "react";

export function InitButton({initFunction}: { initFunction: () => Promise<boolean> }) {
    const [init, setInit] = useState<boolean>(false);

    const runInit = async () => {
        const res = await initFunction();
        setInit(res);
    }

    if (!init) {
        return <BlockerAlertWindow title={'Spiel Starten'} actionText={'Los'} emitter={() => runInit()}/>
    }
    return <></>
}