import {GameSettingInterface} from "@/components/UI/GameSettings/GameSettings";

export function Checkbox({gameSetting, updateSettings, current}: {
    gameSetting: GameSettingInterface,
    updateSettings: (key: string, values: any) => void,
    current?: any
}) {

    return (
        <div className="input-group mb-3 flex-nowrap">
            <label htmlFor={`settings-${gameSetting.key}`}>
            </label>
            <div className="form-check form-switch">
                <input className="form-check-input"
                       checked={!!current}
                       onChange={(e) => updateSettings(gameSetting.key, e.target.checked)}
                       type="checkbox"
                       id={`settings-${gameSetting.key}`}/>
                <label className="form-check-label" htmlFor={`settings-${gameSetting.key}`}>{gameSetting.label}</label>
            </div>
        </div>
    );
}