import {useState} from "react";
import {LocalStorageDatabase} from "@/components/Database/LocalStorageDatabase";
import {v4} from "uuid";
import {Select} from "@/components/UI/GameSettings/Select";
import {GameUtils} from "@/components/Utils/GameUtils";
import {Checkbox} from "@/components/UI/GameSettings/Checkbox";

export interface GameSettingInterface {
    key: string,
    values: any[],
    label: string,
    type: string
}

export function GameSettings({gameName}: { gameName: string }) {

    const [gameSettings, setGameSettings] = useState<any>(null);
    const updateSettings = (settingsKey: string, value: any) => {
        const newSettings = Object.assign({}, gameSettings);
        newSettings[settingsKey] = value;
        setGameSettings(newSettings);
    }

    const createGame = () => {
        const db = new LocalStorageDatabase();
        const rId = v4();
        db.readArray('rounds').then((rounds: any[]) => {
            rounds.push({...gameSettings, id: rId});
            db.writeArray('rounds', rounds).then(() => {
                window.location.href = `/game/${gameName}/${rId}`
            })
        });
    }

    const game = GameUtils.getGameByName(gameName);

    return (
        <div className="container text-center mt-4">
            {
                game.settings.map((e: GameSettingInterface, index: any) => {
                    if (e.type === 'select')
                        return (
                            <Select key={index} gameSetting={e} updateSettings={updateSettings}
                                    current={gameSettings?.[e?.key]}/>
                        )
                    if (e.type === 'checkbox')
                        return (
                            <Checkbox key={index} gameSetting={e} updateSettings={updateSettings}
                                      current={gameSettings?.[e?.key]}/>
                        )
                })
            }

            <br/>
            <button className="btn btn-primary btn-lg" onClick={() => createGame()}>Speichern</button>
        </div>
    )
}