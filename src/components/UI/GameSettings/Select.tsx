import {GameSettingInterface} from "@/components/UI/GameSettings/GameSettings";

export function Select({gameSetting, updateSettings, current}: {
    gameSetting: GameSettingInterface,
    updateSettings: (key: string, values: any) => void,
    current?: any
}) {

    if (!current && !gameSetting.values.includes(''))
        gameSetting.values.push('')
    return (
        <div className="input-group mb-3">
            <label htmlFor={`settings-${gameSetting.key}`}>
            </label>
            <select
                id={`settings-${gameSetting.key}`}
                defaultValue={current ?? ''}
                onChange={(e) => updateSettings(gameSetting.key, e.target.value)}
                className="form-select">
                {gameSetting.values.map((value, index) => {
                    return (
                        <option key={index} value={value}>{value}</option>
                    )
                })}
            </select>
            <span className="input-group-text">{gameSetting.label}</span>
        </div>
    );
}