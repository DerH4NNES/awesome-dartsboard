'use client';
import {BlockerAlertWindow} from "@/components/UI/BlockerAlertWindow";
import {GameUtils} from "@/components/Utils/GameUtils";
import {GenericGameState} from "@/components/Games/GenericGameState";
import {GenericPlayerWinList} from "@/components/Games/Generic/Lists/GenericPlayerWinList";

interface FinishedModal {
    gameState: GenericGameState
}

export function FinishedWindow({gameState}: FinishedModal) {
    const ranking = GameUtils.getPlayerPlacementsByScore(gameState.playerStates);

    return (
        <BlockerAlertWindow title={"Spiel vorbei!"}
                            actionText={"Wiederholen"}
                            emitter={() => window.location.reload()}
                            content={<GenericPlayerWinList players={gameState.players} ranks={ranking}/>}>
        </BlockerAlertWindow>);
}