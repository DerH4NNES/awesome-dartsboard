export function PageHeading({text}: { text: string }) {
    return (
        <span className="display-1 p-4 rounded-end-pill bg-danger text-white">{text}</span>
    );
}