import Link from "next/link";

export function Navigation() {
    return (
        <nav className="px-4 navbar bg-danger navbar-dark navbar-expand-lg navbar-light">
            <a className="navbar-brand" href="#">Awesome Darts</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link className="nav-link" href="/players">Spielerliste</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" href="/games">Spiele</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" href="/settings">Einstellungen</Link>
                    </li>
                </ul>
            </div>
        </nav>
    );
}