class VideoPlayer {

    clips: string[] = [];
    isPlaying = false;

    endVideo(videoElement: HTMLVideoElement) {
        if (videoElement) {
            this.play();
        }
    }

    async play() {
        if (this.clips.length) {
            const clip = this.clips.pop();
            if (clip && !this.isPlaying) {
                console.log("PLAYING");
                this.isPlaying = true;




            }
        }
        else {
            console.log("NO CLIPS");
        }
    }

    addClip(path: string) {
        this.clips.push(path);
        return this;
    }
}

export const VideoService = new VideoPlayer();
