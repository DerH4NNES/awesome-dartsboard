import {GenericPlayerState} from "@/components/Games/Generic/GenericPlayerState";
import {Game01} from "@/components/Games/01/Game01";
import {GameFunctionType} from "@/components/Games/GameFunctionType";

export const GAMES: { [x: string]: GameFunctionType } = {
    '01': Game01
};

export enum SortOrder {
    Descending = 'descending',
    Ascending = 'ascending'
}

export class GameUtils {

    static getGameByName(name: string) {
        return GAMES[name];
    }

    static getPlayerPlacementsByScore(players: GenericPlayerState[], sortOrder: SortOrder = SortOrder.Descending): Map<string, number> {
        const sortedPlayers = players.slice().sort((a, b) => {
            if (sortOrder === SortOrder.Ascending) {
                return a.score - b.score;
            } else {
                return b.score - a.score;
            }
        });

        const placements = new Map<string, number>();
        let currentPlacement = 1;
        let previousScore = sortedPlayers[0].score;
        sortedPlayers.forEach((player, index) => {
            if (index > 0 && player.score !== previousScore) {
                currentPlacement = index + 1;
            }
            placements.set(player.playerId, currentPlacement);
            previousScore = player.score;
        });

        return placements;
    }
}