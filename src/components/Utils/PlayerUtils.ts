import {GenericGameState} from "@/components/Games/GenericGameState";
import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {GenericPlayerState} from "@/components/Games/Generic/GenericPlayerState";
import {GenericRoundScoreInterface} from "@/components/Games/Generic/GenericRoundScoreInterface";

export class PlayerUtils {
    static getCurrentPlayer(gameState: GenericGameState): GenericPlayer {
        return PlayerUtils.getPlayerById(gameState, gameState.currentPlayerUUID);
    }

    static getPlayerById(gameState: GenericGameState, playerId: string): GenericPlayer {
        return gameState?.players?.find((p) => p.uuid === playerId) as GenericPlayer;
    }

    static getPlayerStateByPlayerId(gameState: GenericGameState, playerId: string): GenericPlayerState | undefined {
        return gameState.playerStates.find((e) => e.playerId === playerId);
    }

    static getCurrentPlayerState(gameState: GenericGameState): GenericPlayerState | undefined {
        return gameState.playerStates.find((e) => e.playerId === gameState.currentPlayerUUID);
    }

    static isCurrentPlayer(gameState: GenericGameState, playerUUID: string) {
        return this.getCurrentPlayer(gameState).uuid === playerUUID
    }

    static getPlayerScoreByPlayerId(gameState: GenericGameState, playerId: string) {
        const playerState = this.getPlayerStateByPlayerId(gameState, playerId);
        return playerState?.score;
    }

    static getCurrentPlayerScore(gameState: GenericGameState) {
        const playerState = this.getPlayerStateByPlayerId(gameState, gameState.currentPlayerUUID);
        return playerState?.score;
    }

    static sumScore(roundScore: GenericRoundScoreInterface) {
        const score1 = roundScore?.score1Throw?.value || 0;
        const factor1 = roundScore?.score1Throw?.factor || 0;
        const score2 = roundScore?.score2Throw?.value || 0;
        const factor2 = roundScore?.score2Throw?.factor || 0;
        const score3 = roundScore?.score3Throw?.value || 0;
        const factor3 = roundScore?.score3Throw?.factor || 0;

        if (!roundScore?.valid) {
            return 0;
        }

        return (score1 * factor1) + (score2 * factor2) + (score3 * factor3);
    }

    static getPlayerTempScoreByPlayerState(playerState: GenericPlayerState) {
        return playerState.score - PlayerUtils.sumScore(playerState.getCurrentRound());
    }

    static getPlayerTempScoreByPlayerId(gameState: GenericGameState, playerId: string) {
        const playerState = PlayerUtils.getPlayerStateByPlayerId(gameState, playerId) || new GenericPlayerState(playerId);
        return playerState.score - PlayerUtils.sumScore(playerState.getCurrentRound());
    }

    static validateRoundScore(score: number, roundScore: GenericRoundScoreInterface) {
        if (!roundScore.valid) {
            return false;
        }
        if (score - this.sumScore(roundScore) === 0) {
            return true;
        }

        return score - this.sumScore(roundScore) > -1;
    }

    static updateCurrentPlayerState(gameState: GenericGameState, playerState: GenericPlayerState) {
        return (Object.assign({}, {
            ...gameState,
            playerStates: gameState.playerStates.map((ps) => {
                if (ps.playerId === gameState.currentPlayerUUID)
                    return playerState;
                return ps;
            })
        }));
    }

}