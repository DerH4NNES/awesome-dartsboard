import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";

export class LocalStorageUtils {
    static loadPlayers(): GenericPlayer[] {
        const serializedPlayers = localStorage.getItem('players');
        if (serializedPlayers) {
            const deserializedPlayerData = JSON.parse(serializedPlayers);
            return deserializedPlayerData.map((sP: any) => Object.assign(new GenericPlayer(), sP));
        }
        return [];
    }
}