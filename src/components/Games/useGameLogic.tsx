import {Dispatch, SetStateAction, useEffect} from "react";
import {OnHitEventInterface} from "@/components/Games/Generic/OnHitEventInterface";
import {GenericGameState} from "@/components/Games/GenericGameState";
import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {DartsBoardEvents} from "@/components/Boards/DartsBoardEvents";
import {GenericGameLogic} from "@/components/Games/GenericGameLogic";

// @ts-ignore
export function useGenericGameLogic(gameState: GenericGameState, setGameState:  Dispatch<SetStateAction<GenericGameState>>, players: GenericPlayer[], settings) {


    useEffect(() => {
        const onHit = (hit: OnHitEventInterface) => {
            DartsBoardEvents.emit('ON_PLAYER_HIT', hit, gameState, setGameState)
        };

        const onPressContinue = () => {
            DartsBoardEvents.emit('GAME_PRESS_CONTINUE', gameState, setGameState)
        };

        DartsBoardEvents.on('PRESS_CONTINUE', onPressContinue)
        DartsBoardEvents.on('GAME_PRESS_CONTINUE', GenericGameLogic.ON_PLAYER_CONTINUE)
        DartsBoardEvents.on('ON_HIT', onHit);
        DartsBoardEvents.on('ON_PLAYER_HIT', GenericGameLogic.ON_HIT);
        DartsBoardEvents.on('ROUND_NEXT', GenericGameLogic.ON_ROUND_NEXT);
        DartsBoardEvents.on('GAME_FINISHED', GenericGameLogic.GAME_FINISHED);
        DartsBoardEvents.on('PLAYER_CONTINUE', GenericGameLogic.ON_PLAYER_CONTINUE);
        DartsBoardEvents.on('ON_PLAYER_FINISH', GenericGameLogic.ON_PLAYER_FINISH);


        return () => {
            DartsBoardEvents.off('PRESS_CONTINUE', onPressContinue)
            DartsBoardEvents.off('GAME_PRESS_CONTINUE', GenericGameLogic.ON_PLAYER_CONTINUE)
            DartsBoardEvents.off('ON_HIT', onHit);
            DartsBoardEvents.off('ON_PLAYER_HIT', GenericGameLogic.ON_HIT);
            DartsBoardEvents.off('ROUND_NEXT', GenericGameLogic.ON_ROUND_NEXT);
            DartsBoardEvents.off('GAME_FINISHED', GenericGameLogic.GAME_FINISHED);
            DartsBoardEvents.off('PLAYER_CONTINUE', GenericGameLogic.ON_PLAYER_CONTINUE);

            DartsBoardEvents.off('ON_PLAYER_FINISH', GenericGameLogic.ON_PLAYER_FINISH);

        }
    }, [players, gameState]);


    if (!gameState) {
        return <div className="loader" />;
    }
}