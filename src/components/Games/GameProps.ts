import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";

export interface GameProps {
    players: GenericPlayer[],
    gameSettings: any,
    initButton: any
}