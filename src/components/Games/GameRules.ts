import {GenericGameState} from "@/components/Games/GenericGameState";
import React, {Dispatch, SetStateAction} from "react";
import {OnHitEventInterface} from "@/components/Games/Generic/OnHitEventInterface";
import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {PlayerUtils} from "@/components/Utils/PlayerUtils";
import {GenericPlayerState} from "@/components/Games/Generic/GenericPlayerState";
import {DartsBoardEvents} from "@/components/Boards/DartsBoardEvents";

export enum THROW_STATUS {
    CONTINUE,
    INVALID
}

export abstract class GameRule {
    abstract validate(gameState: GenericGameState, player: GenericPlayer, setGameState: Dispatch<SetStateAction<GenericGameState>>, hit: OnHitEventInterface): THROW_STATUS
}

export class WIN_IF_ZERO_SCORE extends GameRule {
    validate(gameState: GenericGameState, player: GenericPlayer, setGameState: React.Dispatch<React.SetStateAction<GenericGameState>>, hit: OnHitEventInterface): THROW_STATUS {
        const playerState = PlayerUtils.getPlayerStateByPlayerId(gameState, player.uuid) || new GenericPlayerState(player.uuid);
        if (playerState.score - PlayerUtils.sumScore(playerState.getCurrentRound()) === 0) {

            if (!gameState.isDoubleOut) {
                DartsBoardEvents.emit('GAME_FINISHED', gameState, setGameState, player);
                return THROW_STATUS.CONTINUE;
            }

            if (hit.prefix.toLowerCase() === 'd') {
                DartsBoardEvents.emit('GAME_FINISHED', gameState, setGameState, player);
                return THROW_STATUS.CONTINUE;
            }
        }
        return THROW_STATUS.CONTINUE;
    }
}

export class NEXT_PLAYER_IF_TOO_MUCH_01 extends GameRule {
    validate(gameState: GenericGameState, player: GenericPlayer, setGameState: React.Dispatch<React.SetStateAction<GenericGameState>>, hit: OnHitEventInterface): THROW_STATUS {
        const playerState = PlayerUtils.getPlayerStateByPlayerId(gameState, player.uuid) || new GenericPlayerState(player.uuid);

        if (gameState.isDoubleOut && playerState.score - PlayerUtils.sumScore(playerState.getCurrentRound()) === 1) {
            DartsBoardEvents.emit('TOO_MUCH_POINTS', gameState, setGameState, playerState);
            return THROW_STATUS.INVALID;
        }

        if (gameState.isDoubleOut && playerState.score - PlayerUtils.sumScore(playerState.getCurrentRound()) === 0 && hit.prefix.toLowerCase() !== 'd') {
            DartsBoardEvents.emit('TOO_MUCH_POINTS', gameState, setGameState, playerState);
            return THROW_STATUS.INVALID;
        }

        if (playerState.score - PlayerUtils.sumScore(playerState.getCurrentRound()) < 0) {
            DartsBoardEvents.emit('TOO_MUCH_POINTS', gameState, setGameState, playerState);
            return THROW_STATUS.INVALID;
        }


        return THROW_STATUS.CONTINUE;
    }
}

export class NEXT_PLAYER_AFTER_THREE_DARTS extends GameRule {
    validate(gameState: GenericGameState, player: GenericPlayer, setGameState: React.Dispatch<React.SetStateAction<GenericGameState>>, hit: OnHitEventInterface): THROW_STATUS {
        const playerState = PlayerUtils.getPlayerStateByPlayerId(gameState, player.uuid) || new GenericPlayerState(player.uuid);
        if (playerState.getCurrentRound().score3Throw !== undefined) {
            DartsBoardEvents.emit('ON_PLAYER_FINISH', gameState, setGameState);
            return THROW_STATUS.INVALID;
        }
        return THROW_STATUS.CONTINUE;
    }
}

export class END_GAME_IF_ROUNDS_EXCEEDED extends GameRule {
    validate(gameState: GenericGameState, player: GenericPlayer, setGameState: React.Dispatch<React.SetStateAction<GenericGameState>>, hit: OnHitEventInterface): THROW_STATUS {
        if (gameState.round.current >= gameState.round.max) {
            DartsBoardEvents.emit('GAME_FINISHED', gameState, setGameState);
            return THROW_STATUS.INVALID;
        }

        return THROW_STATUS.CONTINUE;
    }
}

export const GameRules = {
    WIN_IF_ZERO_SCORE: new WIN_IF_ZERO_SCORE(),
    NEXT_PLAYER_IF_TOO_MUCH_01: new NEXT_PLAYER_IF_TOO_MUCH_01(),
    NEXT_PLAYER_AFTER_THREE_DARTS: new NEXT_PLAYER_AFTER_THREE_DARTS(),
    END_GAME_IF_ROUNDS_EXCEEDED: new END_GAME_IF_ROUNDS_EXCEEDED()
}
