'use client';
import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {GenericBoardVisual} from "@/components/Boards/GenericBoardVisual";
import {GenericCurrentRound} from "@/components/Games/Generic/GenericCurrentRound";
import {useGameState} from "@/components/Games/01/useGameState";
import {BlockerAlertWindow} from "@/components/UI/BlockerAlertWindow";
import {PlayerUtils} from "@/components/Utils/PlayerUtils";
import {GenericPlayerVisual} from "@/components/Games/Generic/GenericPlayerVisual";
import toast from "react-hot-toast";
import {GenericPlayerList} from "@/components/Games/Generic/GenericPlayerList";
import {FinishedWindow} from "@/components/UI/FinishedModal";
import {GameFunctionType} from "@/components/Games/GameFunctionType";
import {DartsBoardEvents} from "@/components/Boards/DartsBoardEvents";
import {PageHeading} from "@/components/UI/PageHeading";
import {useGame01Logic} from "@/components/Games/01/useGame01Logic";

function game({
                  players,
                  settings = {maxRounds: 25, startScore: 101, isDoubleIn: false, isDoubleOut: true},
                  initButton
              }: {
    players: GenericPlayer[],
    settings?: { maxRounds: number, startScore: number, isDoubleIn: boolean, isDoubleOut: boolean },
    initButton: JSX.Element
}) {

    const {gameState, setGameState} = useGameState(players, settings);
    useGame01Logic(gameState, setGameState, players, settings);

    if (!gameState) {
        return <div className="loader"/>;
    }

    const currentPlayer = PlayerUtils.getCurrentPlayer(gameState);
    const currentPlayerState = PlayerUtils.getPlayerStateByPlayerId(gameState, currentPlayer.uuid);

    if (!currentPlayer || !currentPlayerState) {
        toast.error('Internal error');
        return <div className="loader"/>;
    }

    const score = gameState.blocked ? currentPlayerState.score : PlayerUtils.getPlayerTempScoreByPlayerState(currentPlayerState)

    return (
        <div className='text-start h-100'>
            {gameState.finish && <FinishedWindow gameState={gameState}/>}
            {gameState.blocked && !gameState.finish && <BlockerAlertWindow title={'Nächster Spieler'} actionText={'OK'}
                                                                           emitter={() => DartsBoardEvents.emit('PLAYER_CONTINUE', gameState, setGameState)}/>}
            {initButton}
            <div className="container-fluid h-100 align-middle">
                <div className="position-fixed w-100 top-0 row">
                    <div className="col p-0 text-start">
                        <PageHeading text={game.displayName}/>
                    </div>
                    <div className="col p-0 text-center">
                        {Object.keys(settings)
                            .filter((e) => !['id', 'startScore', 'maxRounds'].includes(e))
                            .map((e, index) => {
                                return <span
                                    // @ts-ignore
                                    className={"badge my-2 p-2 mx-1 " + (settings[e] + '' === 'false' ? 'bg-danger' : 'bg-primary')}
                                    // @ts-ignore
                                    key={index}>{e + ': ' + settings[e]}</span>
                            })}
                    </div>
                    <div className="col p-0 text-white text-end">
                        <span className="display-1 p-4 rounded-start-pill bg-danger text-white">
                            {`${gameState?.round?.current} / ${gameState?.round?.max}`}
                        </span>

                    </div>
                </div>
                <div className="row h-100 align-items-center">
                    <div className="col-4 text-center justify-content-center align-items-center d-flex">
                        <GenericPlayerVisual playerScore={score}/>
                    </div>
                    <div className="col text-center justify-content-center align-items-center mt-4">
                        <GenericBoardVisual/>
                    </div>
                    <div className="col-1 offset-3 d-flex justify-content-end align-items-center">
                        <GenericCurrentRound roundScore={currentPlayerState.getCurrentRound()}/>
                    </div>
                </div>
                <GenericPlayerList players={gameState.players} gameState={gameState}/>
            </div>
        </div>)
}

game.displayName = '01 Game';

game.settings = [
    {
        key: 'startScore',
        type: 'select',
        label: '01 Spiel',
        values: [32, 301, 501, 701, 1001]
    },
    {
        key: 'maxRounds',
        type: 'select',
        label: 'Maximale Runden',
        values: [5, 10, 25, 1000]
    },
    {
        key: 'isDoubleOut',
        type: 'checkbox',
        label: 'Double Out',
        values: []
    },
    {
        key: 'isDoubleIn',
        type: 'checkbox',
        label: 'Double In',
        values: []
    }
];

const Game01: GameFunctionType = game
export {Game01};