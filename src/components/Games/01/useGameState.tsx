import {useLayoutEffect, useState} from "react";
import {GenericGameState} from "@/components/Games/GenericGameState";
import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {GenericPlayerState} from "@/components/Games/Generic/GenericPlayerState";

export function useGameState(players: GenericPlayer[], settings: { maxRounds: any, startScore: any, isDoubleOut: any, isDoubleIn: any }) {
    // @ts-ignore
    const [gameState, setGameState] = useState<GenericGameState>(null);

    useLayoutEffect(() => {
        if (!gameState) {
            const initPlayerStates = players.map((p) => new GenericPlayerState(p.uuid, settings.startScore));
            setGameState({
                round: {
                    max: settings.maxRounds,
                    current: 1
                },
                players: players,
                currentPlayerUUID: players[0].uuid,
                initialized: false,
                blocked: false,
                finish: false,
                playerStates: initPlayerStates,
                isDoubleOut: settings.isDoubleOut,
                isDoubleIn: settings.isDoubleIn,
                gameRules: [
                    'WIN_IF_ZERO_SCORE',
                    'END_GAME_IF_ROUNDS_EXCEEDED',
                    'NEXT_PLAYER_AFTER_THREE_DARTS',
                    'NEXT_PLAYER_IF_TOO_MUCH_01',
                ]
            })
        }
    }, [players]);

    return {gameState, setGameState};
}