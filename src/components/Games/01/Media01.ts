import {MediaPlayer} from "@/components/MediaPlayer";
import {DartsBoardEvents} from "@/components/Boards/DartsBoardEvents";
import {OnHitEventInterface} from "@/components/Games/Generic/OnHitEventInterface";

export function onValidHit(hit: OnHitEventInterface) {

    // Play triple 20 video effect
    if (hit.prefix.toLowerCase() === 't' && hit.value === 20) {
        DartsBoardEvents.emit('MEDIA_PLAY_CLIP', 'https://www.shutterstock.com/shutterstock/videos/1110423005/preview/stock-footage--seconds-countdown-timer-animation-comic-style-countdown-number.webm')
        return;
    }

    // On double
    if (hit.prefix.toLowerCase() === 'd') {
        setTimeout(() => new MediaPlayer().playAudio(), 150);
    }

    // On triple
    if (hit.prefix.toLowerCase() === 't') {
        setTimeout(() => new MediaPlayer().playAudio(), 150);
        setTimeout(() => new MediaPlayer().playAudio(), 300);
    }

    // single hit
    new MediaPlayer().playAudio();
}