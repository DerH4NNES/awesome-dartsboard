import {GenericGameState} from "@/components/Games/GenericGameState";
import {Dispatch, SetStateAction} from "react";
import {GenericPlayerState} from "@/components/Games/Generic/GenericPlayerState";
import {PlayerUtils} from "@/components/Utils/PlayerUtils";
import {DartsBoardEvents} from "@/components/Boards/DartsBoardEvents";
import toast from "react-hot-toast";

export class Game01Logic {

    static ON_TOO_MUCH_POINTS(gameState: GenericGameState, setGameState: Dispatch<SetStateAction<GenericGameState>>, currentPlayerState: GenericPlayerState) {
        currentPlayerState.setCurrentRound({valid: false});
        gameState = PlayerUtils.updateCurrentPlayerState(gameState, currentPlayerState);
        DartsBoardEvents.emit('ON_PLAYER_FINISH', gameState, setGameState);
        toast.error('Überworfen :(');
    }

}