import {Dispatch, SetStateAction, useEffect} from "react";
import {GenericGameState} from "@/components/Games/GenericGameState";
import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {DartsBoardEvents} from "@/components/Boards/DartsBoardEvents";
import {onValidHit} from "@/components/Games/01/Media01";
import {Game01Logic} from "@/components/Games/01/Game01Logic";
import {useGenericGameLogic} from "@/components/Games/useGameLogic";

// @ts-ignore
export function useGame01Logic(gameState: GenericGameState, setGameState: Dispatch<SetStateAction<GenericGameState>>, players: GenericPlayer[], settings) {

    useGenericGameLogic(gameState, setGameState, players, settings);

    useEffect(() => {
        DartsBoardEvents.on('TOO_MUCH_POINTS', Game01Logic.ON_TOO_MUCH_POINTS);
        DartsBoardEvents.on('ON_VALID_HIT', onValidHit);

        return () => {
            DartsBoardEvents.off('TOO_MUCH_POINTS', Game01Logic.ON_TOO_MUCH_POINTS);
            DartsBoardEvents.off('ON_VALID_HIT', onValidHit);


        }
    }, [players, gameState]);


    if (!gameState) {
        return <div className="loader"/>;
    }
}