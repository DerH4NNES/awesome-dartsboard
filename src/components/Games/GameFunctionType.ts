import {GameSettingInterface} from "@/components/UI/GameSettings/GameSettings";
import {GenericPlayer} from "./Generic/GenericPlayer";
import {ReactElement} from "react";

export interface GameProps {
    players: GenericPlayer[],
    settings: any,
    initButton: ReactElement<any, any>
}

export interface GameFunctionType {
    displayName: string;
    settings: GameSettingInterface[]
    ({players, initButton, settings}: GameProps): JSX.Element;
}

