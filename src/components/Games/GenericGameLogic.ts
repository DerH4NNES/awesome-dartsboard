import {GenericGameState} from "@/components/Games/GenericGameState";
import {OnHitEventInterface} from "@/components/Games/Generic/OnHitEventInterface";
import {Dispatch, SetStateAction} from "react";
import {PlayerUtils} from "@/components/Utils/PlayerUtils";
import toast from "react-hot-toast";
import {DartsBoardEvents} from "@/components/Boards/DartsBoardEvents";
import {GameRules, THROW_STATUS} from "@/components/Games/GameRules";


export class GenericGameLogic {

    static parseTrow(gameState: GenericGameState, setGameState: Dispatch<SetStateAction<GenericGameState>>, hit: OnHitEventInterface) {
        const currentPlayer = PlayerUtils.getCurrentPlayer(gameState);

        for (let gameRule of gameState.gameRules) {
            // @ts-ignore
            if (GameRules[gameRule]?.validate(gameState, currentPlayer, setGameState, hit) !== THROW_STATUS.CONTINUE) {
                return false;
            }
        }

        return true
    }

    static ON_HIT(onHitEvent: OnHitEventInterface, gameState: GenericGameState, setGameState: Dispatch<SetStateAction<GenericGameState>>) {

        if (gameState.blocked) {
            return
        }


        const playerState = PlayerUtils.getCurrentPlayerState(gameState);

        if (!playerState) {
            toast.error('Internal Error');
            return;
        }

        if (!playerState.getCurrentRound().score1Throw) {
            playerState.setCurrentRound({score1Throw: onHitEvent})
        } else if (!playerState.getCurrentRound().score2Throw) {
            playerState.setCurrentRound({score2Throw: onHitEvent})
        } else if (!playerState.getCurrentRound().score3Throw) {
            playerState.setCurrentRound({score3Throw: onHitEvent})
        }

        gameState = PlayerUtils.updateCurrentPlayerState(Object.assign({}, gameState), playerState);
        setGameState(gameState);
        if (GenericGameLogic.parseTrow(gameState, setGameState, onHitEvent)) {
            DartsBoardEvents.emit('ON_VALID_HIT', onHitEvent);
        }

        return onHitEvent;
    }

    static GAME_FINISHED(gameState: GenericGameState, setGameState: Dispatch<SetStateAction<GenericGameState>>) {
        const newObj = Object.assign({}, gameState);
        newObj.finish = true;
        setGameState(newObj);
    }

    static ON_PLAYER_FINISH(gameState: GenericGameState, setGameState: Dispatch<SetStateAction<GenericGameState>>) {
        let newObj = Object.assign({}, gameState);
        const playerState = PlayerUtils.getCurrentPlayerState(gameState);

        if (!playerState) {
            toast.error('Internal Error');
            return;
        }

        const currentRound = playerState.getCurrentRound();
        if (PlayerUtils.validateRoundScore(playerState.score, currentRound)) {
            playerState.score -= PlayerUtils.sumScore(currentRound);
            newObj = PlayerUtils.updateCurrentPlayerState(gameState, playerState);
        }

        newObj.blocked = true;
        setGameState(newObj);
    }

    static ON_PLAYER_CONTINUE(gameState: GenericGameState, setGameState: Dispatch<SetStateAction<GenericGameState>>) {
        const newGameState = Object.assign({}, GenericGameLogic.getNextPlayerGameState(gameState, setGameState));
        newGameState.blocked = false;
        setGameState(newGameState);
    }

    static ON_ROUND_NEXT(gameState: GenericGameState, setGameState: Dispatch<SetStateAction<GenericGameState>>) {
        gameState.round.current++;

        if (GameRules.END_GAME_IF_ROUNDS_EXCEEDED.validate(gameState, PlayerUtils.getCurrentPlayer(gameState), setGameState, {} as OnHitEventInterface)) {
            gameState.finish = true;
            gameState.round.current--;
        }
        return gameState;
    }


    static getNextPlayerGameState(gameState: GenericGameState, setGameState: Dispatch<SetStateAction<GenericGameState>>): GenericGameState | undefined {
        let currentPlayerIndex = 0;
        let newGameState = Object.assign({}, gameState);

        for (let counter = 0; counter < gameState.players.length; counter++) {
            if (gameState.players[counter].uuid === gameState.currentPlayerUUID) {
                currentPlayerIndex = counter;
            }
        }

        const playerState = PlayerUtils.getCurrentPlayerState(newGameState);

        if (!playerState) {
            toast.error('Internal error');
            return undefined;
        }
        playerState.addNewRound();
        newGameState = PlayerUtils.updateCurrentPlayerState(newGameState, playerState);

        if (currentPlayerIndex < gameState.players.length - 1) {
            newGameState.currentPlayerUUID = gameState.players[currentPlayerIndex + 1].uuid;
        } else {
            newGameState.currentPlayerUUID = gameState.players[0].uuid;
            newGameState = GenericGameLogic.ON_ROUND_NEXT(newGameState, setGameState);
        }

        return newGameState;
    }
}