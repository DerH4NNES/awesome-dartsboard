import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {GenericPlayerState} from "@/components/Games/Generic/GenericPlayerState";

export interface GenericGameState {
    round: {
        current: number,
        max: number
    },
    currentPlayerUUID: string,
    players: GenericPlayer[],
    playerStates: GenericPlayerState[],
    initialized: boolean,
    finish: boolean,
    blocked: boolean,
    isDoubleIn: boolean
    isDoubleOut: boolean,
    gameRules: string[]
}