export function GenericPlayerVisual({playerScore}: { playerScore: number }) {
    return <span className="display-1"><b>{playerScore}</b></span>
}
