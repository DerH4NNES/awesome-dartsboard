import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {GenericGameState} from "@/components/Games/GenericGameState";
import {PlayerUtils} from "@/components/Utils/PlayerUtils";

export function GenericPlayerList({players, gameState}: {players: GenericPlayer[], gameState: GenericGameState}) {
    return (
        <div className="row position-fixed w-100 bottom-0">
            {
                players?.map((player: GenericPlayer, index: number) => {
                    return (
                        <div key={index}
                             className={(PlayerUtils.isCurrentPlayer(gameState, player.uuid) ? "bg-danger " : 'bg-light ') + "col border-1 border br-0 py-2"}>
                            <div
                                className={(PlayerUtils.isCurrentPlayer(gameState, player.uuid) ? " text-white " : '')}>
                                <div className="text-center">
                                    <span className="mb-0 display-6"><b>{PlayerUtils.getPlayerScoreByPlayerId(gameState, player.uuid)}</b></span>
                                    <h5>{player.name}</h5>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}