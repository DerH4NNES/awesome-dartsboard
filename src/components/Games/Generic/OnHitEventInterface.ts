export interface OnHitEventInterface {
    prefix: string,
    value: number,
    raw: string,
    factor: number
}