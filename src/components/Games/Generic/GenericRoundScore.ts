import {GenericRoundScoreInterface} from "@/components/Games/Generic/GenericRoundScoreInterface";
import {OnHitEventInterface} from "@/components/Games/Generic/OnHitEventInterface";

export class GenericRoundScore implements GenericRoundScoreInterface {
    score1Throw: OnHitEventInterface | undefined;
    score2Throw: OnHitEventInterface | undefined;
    score3Throw: OnHitEventInterface | undefined;
    valid: boolean = true;
}