import {OnHitEventInterface} from "@/components/Games/Generic/OnHitEventInterface";

export interface GenericRoundScoreInterface {
    score1Throw: OnHitEventInterface | undefined;
    score2Throw: OnHitEventInterface | undefined;
    score3Throw: OnHitEventInterface | undefined;
    valid: boolean
}