import {v4} from 'uuid';

export class GenericPlayer {

    uuid: string;

    constructor(readonly name?: string) {
        this.uuid = v4()
    }

    deserialize() {
        return new GenericPlayer()
    }
}