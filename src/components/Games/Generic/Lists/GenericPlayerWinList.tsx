import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {RxAvatar} from "react-icons/rx";

export function GenericPlayerWinList({players, ranks}: { players: GenericPlayer[], ranks: Map<string, number> }) {
    return (
        <div className="ml-0 pl-0 bottom-0 mb mt-5 container">
            <div className="d-flex flex-row justify-content-center">
                {
                    players?.map((player: GenericPlayer, index: number) => {
                        return (
                            <div key={index} className="card p-4 m-2">
                                <div className="image d-flex flex-column justify-content-center align-items-center">
                                    <div className={"btn btn-primary"}
                                         style={{minHeight: '100px', minWidth: '100px'}}>
                                        <RxAvatar size={70}/>
                                    </div>
                                    <span className="mt-3 display-5">{ranks.get(player.uuid)}</span>
                                    <span className="mt-3 display-6">{player.name}</span>
                                </div>
                            </div>
                        )
                    })
                }
            </div>

        </div>
    )
}