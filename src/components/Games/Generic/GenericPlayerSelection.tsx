import {GenericPlayer} from "@/components/Games/Generic/GenericPlayer";
import {RxAvatar} from "react-icons/rx";
import {useEffect, useState} from "react";
import {LocalStorageUtils} from "@/components/Utils/DatabaseUtils";
import {GenericPlayerListSelected} from "@/components/Games/Generic/GenericPlayerListSelected";
import {DartsBoardEvents} from "@/components/Boards/DartsBoardEvents";

export function GenericPlayerSelection({onSelectCallback}: { onSelectCallback: (players: GenericPlayer[]) => void
}) {
    const [selectedPlayers, setSelectedPlayers] = useState<GenericPlayer[]>([]);
    const [players, setPlayers] = useState<GenericPlayer[]>([]);


    useEffect(() => {
        const loadedPlayers = LocalStorageUtils.loadPlayers();
        setPlayers(loadedPlayers);
    }, []);

    const onPlayerDeselect = (playerId: string) => {
        DartsBoardEvents.emit('GAME_SELECT_PLAYER');
        updatePlayers(selectedPlayers.filter((p) => p.uuid !== playerId));
    }

    const onPlayerSelect = (playerId: string) => {
        DartsBoardEvents.emit('GAME_DE_SELECT_PLAYER');
        const player = players.find((p) => p.uuid === playerId);
        if (player instanceof GenericPlayer) {
            selectedPlayers.push(player);
        }
        updatePlayers(selectedPlayers)
    }

    const updatePlayers = (players: GenericPlayer[]) => {
        setSelectedPlayers(Object.assign([], players));
    }

    const onClickPlayer = (playerId: string) => {
        if (isPlayerSelected(playerId)) {
            onPlayerDeselect(playerId);
        } else {
            onPlayerSelect(playerId);
        }
    }

    const getSelectedPlayer = (playerId: string) => {
        return selectedPlayers.find((p) => p.uuid === playerId);
    }

    const isPlayerSelected = (playerId: string) => {
        return !!getSelectedPlayer(playerId);
    }


    return (
        <div className="row ml-0 pl-0 justify-content-start">
            {
                players?.map((player: GenericPlayer, index: number) => {
                    return (
                        <div key={index} className="col mt-4 mb-4 p-3 justify-content-center">
                            <div className="card p-4">
                                <div className="image d-flex flex-column justify-content-center align-items-center">
                                    <button onClick={() => onClickPlayer(player.uuid)}
                                            className={isPlayerSelected(player.uuid) ? "btn btn-primary" : "btn btn-secondary"}
                                            style={{minHeight: '100px', minWidth: '100px'}}>
                                        <RxAvatar size={70}/>
                                    </button>
                                    <span className="name mt-3">{player.name}</span>
                                </div>
                            </div>

                        </div>
                    )
                })
            }
            <button onClick={() => onSelectCallback(selectedPlayers)} className="btn btn-primary btn-lg">Zum Spiel</button>
            <GenericPlayerListSelected players={selectedPlayers} />
        </div>
    )
}