import {GenericRoundScoreInterface} from "@/components/Games/Generic/GenericRoundScoreInterface";
import {OnHitEventInterface} from "@/components/Games/Generic/OnHitEventInterface";

export class GenericPlayerState {

    roundScores: GenericRoundScoreInterface[] = [];
    score: number;

    constructor(readonly playerId: string, score: number = 301) {
        this.score = score;
        this.addNewRound();
    }

    addNewRound() {
        this.roundScores.push({
            score1Throw: undefined,
            score2Throw: undefined,
            score3Throw: undefined,
            valid: true
        });
    }

    getCurrentRound(): GenericRoundScoreInterface {
        return this.roundScores.slice(-1)[0];
    }

    getLastThrownDart(): OnHitEventInterface | undefined {
        const round = this.getCurrentRound();
        if (round.score3Throw) {
            return round.score3Throw;
        }
        if (round.score2Throw) {
            return round.score2Throw;
        }
        if (round.score1Throw) {
            return round.score1Throw;
        }

        return undefined
    }

    setCurrentRound(elems: any) {
        const elem = this.roundScores.pop();
        this.roundScores.push({...elem, ...elems})
    }
}