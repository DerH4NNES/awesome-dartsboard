import {OnHitEventInterface} from "@/components/Games/Generic/OnHitEventInterface";
import {FaCrosshairs} from "react-icons/fa";
import {GenericRoundScoreInterface} from "@/components/Games/Generic/GenericRoundScoreInterface";
import {PlayerUtils} from "@/components/Utils/PlayerUtils";


export function GenericCurrentRound({roundScore}: { roundScore: GenericRoundScoreInterface }) {

    const calculateClassName = (factor: number) => {
        switch (factor) {
            case 1:
                return 'd-flex justify-content-end mb-1'
            case 2:
                return 'd-flex justify-content-end mb-1 text-success '
            default:
                return 'd-flex justify-content-end mb-1 text-danger'
        }
    }

    const createRoundElement = (hit: OnHitEventInterface | undefined) => {
        return (
            <div className={calculateClassName(hit?.factor ?? 1)}>
                <div
                    className="round-score border display-5 text-center justify-content-center align-items-center d-flex">
                    {hit?.raw.toUpperCase() ?? <FaCrosshairs/>}
                </div>
            </div>
        )
    }

    return (
        <div className="col-round-scores">
            <div className="d-flex justify-content-end">
                <div
                    className="sum-round-score border display-5 text-center justify-content-center align-items-center d-flex">
                    <b>{PlayerUtils.sumScore(roundScore)}</b>
                </div>
            </div>
            {
                createRoundElement(roundScore?.score1Throw)
            }
            {
                createRoundElement(roundScore?.score2Throw)
            }
            {
                createRoundElement(roundScore?.score3Throw)
            }
        </div>
    )
}


