import {DatabaseInterface} from "@/components/Database/DatabaseInterface";

export class LocalStorageDatabase implements DatabaseInterface {
    async readObject(key: string, defaultValue?: object): Promise<object> {
        const value = localStorage.getItem(key);

        if (!value) {
            await this.writeObject(key, defaultValue ?? {});
            return Promise.resolve(defaultValue ?? {});
        }

        return Promise.resolve(JSON.parse(value));
    }

    async readArray(key: string): Promise<[]> {
        const value = localStorage.getItem(key);

        if (!value) {
            await this.writeObject(key, []);
            return Promise.resolve([]);
        }

        return Promise.resolve(JSON.parse(value));
    }

    async writeArray(key: string, object: object[]): Promise<void> {
        localStorage.setItem(key, JSON.stringify(object));
    }
    async writeObject(key: string, object: object): Promise<void> {
        localStorage.setItem(key, JSON.stringify(object));
    }
}