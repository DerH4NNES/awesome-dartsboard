export interface DatabaseInterface {
    writeObject(key: string, object: object): Promise<void>

    readObject(key: string, defaultValue?: object): Promise<object>
}