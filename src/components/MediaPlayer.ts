export class MediaPlayer {
    playAudio(audioPath: string = '/sounds/hit.wav') {
        new Audio(audioPath).play();
    }
}